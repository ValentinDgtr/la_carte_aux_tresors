# La Carte aux Trésors

Bienvenue sur le dépôt du projet 'La Carte aux Trésors'. 

## Description du projet

Ce projet est un exercice proposé par Carbon IT visant à simuler un jeu de plateau dans lequel des aventuriers partent en quête de trésors. 

## Installation

Pour installer ce projet, vous devez suivre les étapes suivantes:

1. Cloner le dépôt:
```bash
git clone https://gitlab.com/ValentinDgtr/la_carte_aux_tresors.git
