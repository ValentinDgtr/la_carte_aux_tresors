const GameSimulation = require('../src/controllers/GameSimulation.js');
const Map = require('../src/models/map.js');
const Adventurer = require('../src/models/adventurer.js');

describe('GameSimulation', () => {
    let gameSimulation;
    beforeEach(() => {
        gameSimulation = new GameSimulation();
    });

    describe('createMap', () => {
        test('it should create a 2D map based on the provided input data', () => {
            const inputData = ['C - 2 - 2', 'T - 3 - 2'];
            // const expectedMap = [['P', 'P'], ['P', 'P']];
            const expectedMap = new Map([['P', 'P'], ['P', 'P']]);


            expect(gameSimulation.createMap(inputData)).toEqual(expectedMap);
        });

        test('it should skip lines that do not start with C', () => {
            const inputData = ['A - 2 - 2', 'C - 3 - 2', 'B - 4 - 4', 'Z - 2 - 2'];
            const expectedMap = new Map([['P', 'P', 'P'], ['P', 'P', 'P']]);

            expect(gameSimulation.createMap(inputData)).toEqual(expectedMap);
        });

        test('it should skip lines with invalid dimensions', () => {
            const inputData = ['C - abc - 2', 'C - 3 - 2', 'C - 2 - def'];
            const expectedMap = new Map([['P', 'P', 'P'], ['P', 'P', 'P']]);

            expect(gameSimulation.createMap(inputData)).toEqual(expectedMap);
        });

        test('it should handle an empty input array', () => {
            const inputData = [];
            const expectedMap = null;

            expect(gameSimulation.createMap(inputData)).toEqual(expectedMap);
        });
    });


    describe('completeMap', () => {
        test('it should set correct cells for mountains and treasures in the map', () => {
            const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P']]);
            const inputData = ['M - 3 - 0', 'T - 1 - 1 - 2'];
            const adventurers = [];
            const expectedMap = new Map([['P', 'P', 'P', 'M'], ['P', 'T2', 'P', 'P']]);

            expect(gameSimulation.completeMap(map, inputData, adventurers)).toEqual(expectedMap);
        });

        test('it should set incorrect cells for mountains and treasures in the map', () => {
            const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P']]);
            const inputData = ['M - 5 - 0', 'T - 4 - 1 - 2'];
            const adventurers = [];
            const expectedMap = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P']]);

            expect(gameSimulation.completeMap(map, inputData, adventurers)).toEqual(expectedMap);
        });
    });


    describe('getAdventurer', () => {
        test('should return an array of adventurer objects', () => {
            const inputData = ['A-John-2-3-N-AAA', 'A-Lola-1-3-S-DAGGA'];

            const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P']]);
            const expectedResult = [new Adventurer('John', 2, 3, 'N', 'AAA'), new Adventurer('Lola', 1, 3, 'S', 'DAGGA')];

            expect(gameSimulation.getAdventurer(map, inputData)).toEqual(expectedResult);
        });

        test('should ignore one of the adventurers', () => {
            const inputData = ['A-John-2-3-N-AAA', 'A-Lola-6-3-S-DAGGA'];

            const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P']]);
            const expectedResult = [new Adventurer('John', 2, 3, 'N', 'AAA')];

            expect(gameSimulation.getAdventurer(map, inputData)).toEqual(expectedResult);
        });

        
        test('return null array', () => {
            const inputData = ['A-John-B-C-N-AAA'];

            const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P']]);
            const expectedResult = [];

            expect(gameSimulation.getAdventurer(map, inputData)).toEqual(expectedResult);
        });
    });









});

