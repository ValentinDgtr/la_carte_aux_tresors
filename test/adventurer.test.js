const Adventurer = require('../src/models/adventurer.js');
const Map = require('../src/models/map.js');

describe('Adventurer', () => {

    test('moves forward when action is A', () => {
        const adventurer = new Adventurer('John', 2, 2, 'N', 'AAA');
        const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P'], ['P', 'P', 'P', 'P']]);
        adventurer.move(map, 'A');

        expect(adventurer.x).toBe(2);
        expect(adventurer.y).toBe(1);
    });

    test("don't move forward if there's a mountain ahead", () => {
        const adventurer = new Adventurer('John', 2, 2, 'N', 'AAA');
        const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'M', 'P'], ['P', 'P', 'P', 'P']]);
        adventurer.move(map, 'A');

        expect(adventurer.x).toBe(2);
        expect(adventurer.y).toBe(2);
    });

    test("don't move forward if it leaves the map", () => {
        const adventurer = new Adventurer('John', 3, 2, 'E', 'AAA');
        const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'M', 'P'], ['P', 'P', 'P', 'P']]);
        adventurer.move(map, 'A');

        expect(adventurer.x).toBe(3);
        expect(adventurer.y).toBe(2);
    });

    test('rotates right when action is D', () => {
        const adventurer = new Adventurer('John', 3, 2, 'E', 'AAA');
        const map = new Map([['P', 'P', 'P', 'P'], ['P', 'P', 'M', 'P'], ['P', 'P', 'P', 'P']]);
        adventurer.move(map, 'D');

        expect(adventurer.direction).toBe('S');
      });
});