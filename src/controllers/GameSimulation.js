const fs = require('fs').promises;
const Map = require('../models/map.js');
const Adventurer = require('../models/adventurer.js');

class GameSimulation {

    async readFilesContent(pathFile) {
        try {
            const inputData = await fs.readFile(pathFile, { encoding: 'utf8' });
            return inputData.split('\n');
        } catch (error) {
            console.error('Error reading file:', error);
            return null;
        }
    }

    createMap(inputData) {
        let map = [];

        for (const line of inputData) {
            if (line.startsWith('C')) {
                const [, x, y] = line.split('-').map(Number);

                if (!isNaN(x) && !isNaN(y)) {
                    const rows = new Array(y).fill(null).map(() => new Array(x).fill('P'));
                    map.push(...rows);
                }
            }
        }

        return map.length > 0 ? new Map(map) : null;
    }

    completeMap(map, inputData, adventurers) {
        for (const line of inputData) {
            if (line.startsWith('M') || line.startsWith('T')) {
                const [firstChar, ...coordinates] = line.split('-').map(item => item.trim());
                const [coordinateX, coordinateY, nbTreasure] = coordinates.map(Number);

                if (!isNaN(coordinateX) && !isNaN(coordinateY) && coordinateX < map.getWidth() && coordinateY < map.getHeight()) {
                    const cellValue = firstChar === 'M' ? 'M' : 'T' + nbTreasure;
                    map.setCell(coordinateX, coordinateY, cellValue);
                }
            }
        }

        for (const adventurer of adventurers) {
            map.setCell(adventurer.x, adventurer.y, 'A');
        }

        return map;
    }

    getAdventurer(map, inputData) {
        const adventurers = [];

        for (const line of inputData) {
            if (line.startsWith('A')) {
                const [, name, x, y, direction, actions] = line.split('-').map(item => item.trim());
                const coordinateX = parseInt(x);
                const coordinateY = parseInt(y);

                if (!isNaN(coordinateX) && !isNaN(coordinateY) && coordinateX < map.getWidth() && coordinateY < map.getHeight()) {
                    const adventurer = new Adventurer(name, coordinateX, coordinateY, direction, actions);
                    adventurers.push(adventurer);
                }
            }
        }

        return adventurers;
    }

    countNbSequence(adventurers) {
        let nbSequence = 0;

        adventurers.forEach(adventurer => {
            if (nbSequence < adventurer.actions.length)
                nbSequence = adventurer.actions.length;
        });

        return nbSequence;
    }

    launchSimulation(map, adventurers, nbSequences) {
        for (let sequence = 0; sequence < nbSequences; sequence++) {
            adventurers.forEach((adventurer) => {
                adventurer.move(map, adventurer.actions.charAt(sequence));
            })
        }

        return map;
    }

    writeOutput(map, adventurers) {
        const casesMountains = map.getMountains();
        const casesTreasures = map.getTreasures();

        const contentOutputFile = `
C - ${map.grid[0].length} - ${map.grid.length}
${casesMountains.map((mountain) => `M - ${mountain[0]} - ${mountain[1]}`).join('\n')}
# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}
${casesTreasures.map((treasure) => `T - ${treasure[0]} - ${treasure[1]} - ${treasure[2]}`).join('\n')}
# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}
${adventurers.map((adventurer) => `A - ${adventurer.name} - ${adventurer.x} - ${adventurer.y} - ${adventurer.direction} - ${adventurer.treasures}`).join('\n')}
                `

        fs.writeFile('output.txt', contentOutputFile, (err) => {
            if (err) {
                console.error('Une erreur est survenue lors de l\'écriture du fichier :', err);
                return;
            }

            console.log('Le fichier a été écrit avec succès.');
        });
    }
};

module.exports = GameSimulation;

