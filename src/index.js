const GameSimulation = require('./controllers/GameSimulation.js');

async function main() {
    try {
        const gameController = new GameSimulation();
        const inputData = await gameController.readFilesContent('input.txt');

        if (!inputData) throw new Error('Could not read file');

        let map = gameController.createMap(inputData);
        if (!map) throw new Error('Could not create map');

        const adventurers = gameController.getAdventurer(map, inputData);
        map = gameController.completeMap(map, inputData, adventurers);

        const nbSequences = gameController.countNbSequence(adventurers);
        map = gameController.launchSimulation(map, adventurers, nbSequences);

        gameController.writeOutput(map, adventurers);
    } catch (error) {
        console.error(error.message);
    }
}

main();