class Adventurer {
    constructor(name, x, y, direction, actions) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.actions = actions
        this.treasures = 0;
    }

    move(map, action) {
        let [newX, newY] = [this.x, this.y];
        const directions = { 'N': [0, -1], 'E': [1, 0], 'S': [0, 1], 'W': [-1, 0] };
        const rotateRight = { 'N': 'E', 'E': 'S', 'S': 'W', 'W': 'N' };
        const rotateLeft = { 'N': 'W', 'W': 'S', 'S': 'E', 'E': 'N' };
    
        if (action === 'A') {
            [newX, newY] = [newX + directions[this.direction][0], newY + directions[this.direction][1]];
    
            if (newX < 0 || newY < 0 || newX >= map.getWidth() || newY >= map.getHeight() || ['M', 'A'].includes(map.getCell(newX, newY))) {
                console.log('Move blocked');
                return;
            }
    
            let cellContent = map.getCell(newX, newY);
            if (cellContent.includes('T')) {
                this.treasures += 1;
                let nbTreasures = parseInt(cellContent.charAt(1));
                if (nbTreasures > 0) {
                    nbTreasures--;
                    map.setCell(newX, newY, 'A' + nbTreasures);
                } else {
                    map.setCell(newX, newY, 'P');
                }
            } else if (cellContent === 'P') {
                map.setCell(newX, newY, 'A');
            }
    
            let oldCellContent = map.getCell(this.x, this.y);
            map.setCell(this.x, this.y, oldCellContent.charAt(1) > 0 ? 'T' + oldCellContent.charAt(1) : 'P');
    
            [this.x, this.y] = [newX, newY];
        } else if (action === 'D') {
            this.direction = rotateRight[this.direction];
        } else if (action === 'G') {
            this.direction = rotateLeft[this.direction];
        }
    }
    
}

module.exports = Adventurer;
