class Map {
    constructor(grid) {
        this.grid = grid;
    }

    getCell(x, y) {
        return this.grid[y][x];
    }

    setCell(x, y, value) {
        this.grid[y][x] = value;
    }

    getHeight(){
        return this.grid.length;
    }

    getWidth(){
        return this.grid[0].length;
    }

    getMountains() {
        let casesMountain = [];
        for (let x = 0; x < this.grid.length; x++) {
            for (let y = 0; y < this.grid[x].length; y++) {
                if (this.grid[x][y] === 'M') {
                    casesMountain.push([y, x]);
                }
            }
        }

        return casesMountain;
    }

    getTreasures() {
        let casesTreasures = [];
        for (let x = 0; x < this.grid.length; x++) {
            for (let y = 0; y < this.grid[x].length; y++) {
                if (this.grid[x][y].charAt(1) > 0) {
                    casesTreasures.push([y, x, this.grid[x][y].charAt(1)]);
                }
            }
        }

        return casesTreasures;
    }
}

module.exports = Map;


